import configparser

import xlwt
import xlrd
import time
from xlutils.copy import copy

from selenium import webdriver
from selenium.webdriver.common.by import By

click_by_xpath = lambda xpath: browser.find_element_by_xpath(xpath).click()


def init():
    """
    Подготовка к запуску
    Читает с config.ini лоин и пароль и подготавливает Selenium для работы
    """
    global browser, LOGIN, PASSWORD
    config = configparser.ConfigParser()
    config.read('config.ini')
    LOGIN = config["Auth"]["login"]
    PASSWORD = config["Auth"]["password"]

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('window-size=1200x600')
    browser = webdriver.Chrome(chrome_options=options)
    #browser = webdriver.Chrome()

def eisz_auth():
    """
    Вход в портал ЕИСЗ с помощью логина и пароля
    """
    browser.get("https://www.eisz.kz/login")
    login_inputbox = browser.find_element(By.ID, value="UserName")
    login_inputbox.send_keys(LOGIN)

    password_inputbox = browser.find_element(By.ID, value="Password")
    password_inputbox.send_keys(PASSWORD)

    click_by_xpath('//*[@id="login-box"]/div/div[1]/form/fieldset/div[2]/button')


def enter_to_rpn():
    """
    Вход в РПН в портале ЕИСЗ
    """
    rpn_button_xpath = '/html/body/div[2]/div[1]/div[1]/ul/li[10]/a/span[2]'
    click_by_xpath(rpn_button_xpath)

    time.sleep(5)
    vvedenie_fiz_lic_xpath='/html/body/div[3]/div[3]/ul/li[1]'
    click_by_xpath(vvedenie_fiz_lic_xpath)


def search_iin_by_fio(text_to_search):
    """
    Поиск людей в РПН
    """
    time.sleep(1)
    browser.find_element_by_xpath('/html/body/div[3]/div[4]/form/div/div/div/input').send_keys(text_to_search)  
    
    time.sleep(1)
    click_by_xpath('//*[@id="btSearchFiz"]')


def process_excel_file(filename):
    """
    Чтение с Excel файла ФИО или ИИН, поиск в РПН людей и заполнение обратно в Excel таблицу output.xls
    """
    excel_file = xlrd.open_workbook(filename, formatting_info=True)
    excel_sheet = excel_file.sheet_by_index(0)
    new_workbook = copy(excel_file)                                                      
    sheet = new_workbook.get_sheet(0)

    for i in range(excel_sheet.nrows):
        fio_or_iin = excel_sheet.row_values(i)[0]
        print(fio_or_iin,'Проверено ', i+1)
        if not isinstance(fio_or_iin, str):
            fio_or_iin = "{:.0f}".format(fio_or_iin)
    
        search_iin_by_fio(fio_or_iin)    
        time.sleep(5)
        error_text = browser.find_element_by_xpath('/html/body/div[3]/div[3]/ul').text
        if error_text != '':
            sheet.write(i, 2, error_text)
            new_workbook.save('output.xls')
            browser.find_element_by_xpath('/html/body/div[3]/div[4]/form/div/div/div/input').clear()
            continue

        fiz_lico_info_xpath = '/html/body/div[3]/div[7]/div/table/tbody/tr/td[2]'
        fiz_lico_data = browser.find_element_by_xpath(fiz_lico_info_xpath).text
        fio_xpath = '/html/body/div[3]/div[7]/div/div/h3'
        fio = browser.find_element_by_xpath(fio_xpath).text
        iin_xpath = '/html/body/div[3]/div[7]/div/table/tbody/tr/td[1]/div[1]/label'
        iin = browser.find_element_by_xpath(iin_xpath).text
            
    
        if fiz_lico_data[:23] == "Сведения о прикреплении":
            _, _, med_org, prichina, data_prikrepleniya, *_ = fiz_lico_data.splitlines()
            sheet.write(i, 0, fio)
            sheet.write(i, 1, iin)
            sheet.write(i, 2, med_org)
            sheet.write(i, 3, prichina)
            sheet.write(i, 4, data_prikrepleniya)
        elif fiz_lico_data[:7] == "Умерший":
            _, death_info, *_ = fiz_lico_data.splitlines()
            sheet.write(i, 0, fio)
            sheet.write(i, 1, iin)
            sheet.write(i, 2, death_info)
        else:
            sheet.write(i, 0, fio)
            sheet.write(i, 1, iin)
            sheet.write(i, 2, fiz_lico_data)
        browser.find_element_by_xpath('/html/body/div[3]/div[4]/form/div/div/div/input').clear()
        new_workbook.save('output.xls')


if __name__ == '__main__':
    init()
    eisz_auth()
    enter_to_rpn()
    process_excel_file('test.xls')
